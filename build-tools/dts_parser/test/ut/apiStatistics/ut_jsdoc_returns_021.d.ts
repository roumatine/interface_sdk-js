/**
 * the ut for jsdoc about returns
 *
 */
export namespace test {
  /**
   * @returns { string } The return value description.
   */
  function func(str: string): string;
}
