/**
 * the ut for method in interface
 *
 */
export interface Test {
  onRequestRedirected?: (str: string) => void;
}
