/**
 * the ut for const in namespace, has the tag of constant and initializer
 */
export namespace test {
  /**
   * @constant
   */
  const name: string = '2';
}