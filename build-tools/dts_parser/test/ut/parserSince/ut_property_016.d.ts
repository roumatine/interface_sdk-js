/**
 * the ut for property in interface, the property has permission,
 * and permission value is not null
 */
export namespace test {
  /**
   * @permission ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  const name: string
}