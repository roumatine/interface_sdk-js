/**
 * the ut for enum, all enum values is null
 */
export enum ErrorCode {
  PERMISSION_DENY,
  ABILITY_NOT_FOUND,
}