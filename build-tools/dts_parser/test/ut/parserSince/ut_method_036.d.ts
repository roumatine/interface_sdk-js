/**interface
 * the ut for method in interface, method is promise
 */
export interface Test {
  test(param1: string): Promise<Want>;
}