/**
 * the ut for jsdoc about permission
 *
 */
export namespace test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS or ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  function func(str: string): void;
}
