/**
 * the ut for jsdoc about systemapi
 *
 */
export class Test {
  /**
   * @systemapi
   */
  func(str: string): void;
}
